import numpy as np


class Layer:
    def __init__(self, input_size: int, output_size: int):
        self.weights: np.ndarray = np.random.randn(input_size, output_size)
        self.bias: np.ndarray = np.zeros((1, output_size))
        self.inputs: np.ndarray = np.empty((0, input_size))
        self.outputs: np.ndarray = np.empty((0, output_size))
        self.d_weights: np.ndarray = np.empty((input_size, output_size))
        self.d_bias: np.ndarray = np.empty((1, output_size))

    def forward(self, inputs: np.ndarray) -> np.ndarray:
        self.inputs = inputs
        self.outputs = np.dot(inputs, self.weights) + self.bias
        return self.outputs

    def backward(self, d_outputs, learning_rate) -> np.ndarray:
        d_inputs = np.dot(d_outputs, self.weights.T)

        self.d_weights = np.dot(self.inputs.reshape(-1, 1), d_outputs)

        self.d_bias = d_outputs

        self.weights -= learning_rate * self.d_weights / np.sqrt(1 + (self.d_weights**2).sum())
        self.bias -= learning_rate * self.d_bias / np.sqrt(1 + (self.d_weights**2).sum())

        return d_inputs
