import numpy as np

from .activation_function import ActivationFunction


class Activation:
    def __init__(self, activation_func: ActivationFunction):
        self.inputs = None
        self.activation_func = activation_func

    def forward(self, inputs: np.ndarray) -> np.ndarray:
        self.inputs = inputs
        return self.activation_func.activate(inputs)

    def backward(self, d_outputs: np.ndarray, learning_rate: float) -> np.ndarray:
        if self.inputs is None:
            raise Exception("No inputs provided")
        return d_outputs * self.activation_func.derivative(self.inputs)
