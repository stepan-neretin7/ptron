import numpy as np

from .activation_function import ActivationFunction


class SigmoidActivation(ActivationFunction):
    def activate(self, x: np.ndarray) -> np.ndarray:
        return 1 / (1 + np.exp(-x))

    def derivative(self, x: np.ndarray) -> np.ndarray:
        sigmoid_x = self.activate(x)
        return sigmoid_x * (1 - sigmoid_x)
