import numpy as np


def standardize_data(data):
    mean_data = np.mean(data, axis=0)
    std_data = np.std(data, axis=0)
    standardized_data = (data - mean_data) / std_data
    return standardized_data


def custom_train_test_split(x, y, test_size=0.2, random_state=None):
    if random_state is not None:
        np.random.seed(random_state)

    indices = np.arange(len(x))
    np.random.shuffle(indices)

    test_samples = int(len(x) * test_size)

    test_indices = indices[:test_samples]
    train_indices = indices[test_samples:]

    x_train, x_test = x.iloc[train_indices], x.iloc[test_indices]
    y_train, y_test = y.iloc[train_indices], y.iloc[test_indices]

    return x_train, x_test, y_train, y_test
