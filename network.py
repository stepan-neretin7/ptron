import numpy as np


class NeuralNetwork:
    def __init__(self, layers):
        self.layers = layers

    def forward_propagation(self, x):
        for layer in self.layers:
            x = layer.forward(x)
        return x

    def back_propagation(self, d_output, learning_rate):
        for layer in reversed(self.layers):
            print(layer)
            layer.backward(d_output, learning_rate)

    @staticmethod
    def mse(y_true, y_pred):
        return ((y_true - y_pred) ** 2).sum()

    @staticmethod
    def accuracy_classification(y_true, y_pred):
        return np.mean(y_true == np.round(y_pred))

    @staticmethod
    def accuracy_regression(y_true, y_pred):
        deviations = np.abs(y_pred - y_true)
        true_absolute = np.abs(y_true)
        accuracies = deviations / true_absolute
        return 1 - np.mean(accuracies)

    def fit(self, x, y, learning_rate, epochs):
        cf = 1
        for epoch in range(epochs):
            total_error = 0
            for i in range(len(x)):
                outputs = self.forward_propagation(x[i])
                error = self.mse(y[i], outputs)
                total_error += error
                d_output = outputs - y[i]
                self.back_propagation(d_output, cf * learning_rate)
            mean_error = total_error / len(x)
            if epoch % 10 == 0:
                cf *= 0.8
            print(f'Epoch {epoch + 1}/{epochs}, Mean Squared Error: {mean_error}')

    def predict(self, x):
        output = self.forward_propagation(x)
        return output
